
# coding: utf-8

# # Import of librairies 

# In[2]:


import matplotlib.pyplot as plt 
import pandas as pd 
import numpy as np 
from pprint import pprint
import csv


# # Loading of librairies

# In[3]:


try:
    df1 = pd.read_csv('/Users/jordangac/Documents/M1/Introduction_to_Big_Data/ProjetBIGDATA/consommation-electrique-par-secteur-dactivite-commune.csv', sep=';', engine='python', encoding='UTF-8')
    df2 = pd.read_csv('/Users/jordangac/Documents/M1/Introduction_to_Big_Data/ProjetBIGDATA/enquetes-gares-connexions-repartition-repartition-par-classe-dage.csv', sep=';', engine='python', encoding='UTF-8') 

except (csv.Error):
    pprint("Error during the file loading")

# Sources of datasets 
# https://www.europeandataportal.eu/data/fr/dataset/repartition-des-clients-par-age-enquetes-en-gare
# https://data.enedis.fr/explore/dataset/consommation-electrique-par-secteur-dactivite-commune/


# # Working with first dataset from ENEDIS

# In[4]:


# Printing information from first dataset 
df1.info()


# # Dropping French regions which are not near the sea exclusion of Ile-de-France

# In[5]:


to_del = df1[df1['Nom région'].isin(['Grand-Est', 'Centre-Val de Loire', 'Bourgogne-Franche-Comté', 'Auvergne-Rhône-Alpes'])].index.tolist() 
df1 = df1.drop(to_del)
df1


# # Mean of the professional electric consumption by region 

# In[7]:


df_groupby_region_mean = df1.groupby('Nom région').mean()

df_sorted_profelec = df_groupby_region_mean.sort_values('Conso totale Professionnel (MWh)', ascending=False)

df_sorted_profelec


# # Plotting the mean of professional consumption by region 

# In[8]:


df_sorted_profelec['Conso totale Professionnel (MWh)'].plot(kind='bar')
plt.show()


# # Sum of the number of residential sites by region 

# In[9]:


df_groupby_region_mean_nbres = df1.groupby('Nom région').sum()

df_sorted_nbres = df_groupby_region_mean_nbres.sort_values('Nb sites Résidentiel', ascending=False)

pprint(df_sorted_nbres['Nb sites Résidentiel'])


# # Plotting the sum of number of residential sites by region

# In[10]:


get_ipython().magic('matplotlib inline')

color = 'green'
df_sorted_nbres['Nb sites Résidentiel'].plot(kind='bar', stacked =True, color=color)

plt.show()


# # # Working from the second dataset from http://europeandataportal.eu
# This dataset group data from several interviews in stations and airports in France about age of travellers

# In[11]:


df_ageRange = df2.groupby("Classe d'âge").mean()

df_sorted_ageRange = df_ageRange.sort_values('Pourcentage', ascending=False)

liste_ageRange = df_sorted_ageRange['Pourcentage'].tolist()
labels = '20 - 29 ans', 'Non renseigné', '30 - 39 ans', '40 - 49 ans', '50 - 59 ans', '60 ans et plus', '19 ans et moins' # Have entered manually because of encoding problems

plt.pie(liste_ageRange, labels=labels, shadow=True, autopct='%1.1f%%', startangle=90)
plt.axis('on')
pprint(df_sorted_ageRange)

# We can see that the range 20 - 29 years old is that one which travel the most, we can conclude that young people travel by group of 3 people in mean, the most are single or in a relationship without children so they go in vacation with few people due to expensive price of vacation (in France) and the beginning of career of young people (and their following salaries)

# Then, the adequate residence for this type of people seems to be these one which have an apartment area between 40 m2 and 60 m2 
# # Sorting and plotting the area apartment

# In[14]:



df_sorted_areaApart = df_groupby_region_mean.sort_values('Superficie des logements 40 à 60 m2', ascending=False)
print(df_sorted_areaApart['Superficie des logements 40 à 60 m2'])
df_sorted_areaApart['Superficie des logements 40 à 60 m2'].plot(kind='bar', color = 'red')


# # The Ranker Dataset 
# We have decided to create a new datafrane with all ranks of previous results. The system is like in sports ranking, there is in our study 8 regions :
#          1st has 10 pts             5th has 4 pts
#          2nd has 8 pts              6th has 3 pts
#          3rd has 6 pts              7th has 2 pts
#          4th has 5 pts              8th has 1 pt 
# In[15]:


# Creation of ranking dataframe #

liste_region = df1['Nom région'].drop_duplicates().tolist()
liste_region.sort()
rank_array = [3,6,8,5,7,4,2,1]
df_ranking = pd.DataFrame(rank_array, index= liste_region, columns=['Rank conso pro'])
rank_array2 = [7,5,8,2,3,6,4,1]
rank_array3 = [3,6,4,8,5,7,1,2]
df_ranking['Rank nb residential'] = rank_array2
df_ranking['Rank area apartment'] = rank_array3

# Creating fonction to attribute points for each region #
liste_points = [10, 8, 6, 5, 4, 3, 2, 1]
new_ranking = df_ranking.sort_values('Rank conso pro', ascending=True)
new_ranking['Points by rank'] = liste_points
total_points = [28, 23, 14, 10, 13, 10, 12, 7]
new_ranking['Total points'] = total_points

new_ranking['Final rank'] = new_ranking['Total points'].rank(ascending=False)
final_ranking = new_ranking.sort_values('Final rank', ascending=True)

final_ranking

# As we can see the best region to have vacations is 'Ile-de-France' but it's not near the sea, due to its important develop;ent and culture of Paris we advise to customers to go to this location if they're not from Paris. Otherwise, we advise them to go to 'Provence-Alpes-Cote d'Azur